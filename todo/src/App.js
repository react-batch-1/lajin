import React, { Component } from 'react';


class AddTodo extends Component {


  render() {
    return (
      <>
      <div>
        <input type='text' placeholder='Add your task' name='new-todo' maxlength="200" size="50" />
        <button className='button' onClick={this.props.onAddTask}>Add Task</button>
      </div>
      
      </>
    );
  }
}

class TodoList extends Component {

  constructor(props) {
    super(props);
    this.state = {
      tasks: [
        {id: 1,task:"Task One",status: true},
        {id: 2,task:"Task Two",status: false},
        {id: 3,task:"Task Three",status: true},
      ]
    };
  }
  /*addTask = (task) => {
    this.setState({ tasks: [...this.state.tasks,task] });
    console.log(this.state.tasks)
  }*/
  render() {
    return (
      <>
      <AddTodo onAddTask={this.addTask}/>
      <ul class="list">
        {
          this.state.tasks.map((item)=>{
            return <li id={item.id}>{item.task}</li>;
          })
        
        }
      </ul>

      </>

    )
  }
}

export default TodoList;


