import React, { Component } from 'react';

import TodoForm from './TodoForm';

class Todo extends Component {

  constructor(props) {
    super(props);
    this.state = {
      tasks: [],
      completedTasks:[]
    };
  }

  addTask = (task) => {
    this.setState({ tasks: [...this.state.tasks,task] });
  }

  removeTask = (taskId)=>{
    const newArr = this.state.tasks.filter((task) => task.id !== taskId );
    this.setState({tasks:newArr});
  }

  completeTask = (taskId)=>{
 
    this.state.tasks.forEach((task) => {
      if(task.id === taskId) {
        task.status = true;
        //this.setState({ completedTasks: [...this.state.completedTasks,task] });
      }        
    });
    this.setState(this.state.tasks);
    console.log(this.state.tasks);
    
  }

  render() {
    return (
      <>
      <TodoForm onAddTask={this.addTask} />
      <h2>Pending Tasks</h2>
      <ul class="list">
        {
          this.state.tasks.map((item)=>{
            return (!item.status?<li id={item.id}>{item.task} <button className="green" onClick={() => this.completeTask(item.id)}>Done</button> <button className="yellow">Edit</button> <button className="red" onClick={() => this.removeTask(item.id)}>Delete</button></li>:"");
          })
        
        }
      </ul>

      

      <h2>Comleted Tasks</h2>
      <ul class="list-done">
        {
          this.state.tasks.map((item)=>{
            return (item.status?<li id={item.id}>{item.task}</li>:"");
          })
        }
      </ul>
      </>

    )
  }
}


export default Todo;
