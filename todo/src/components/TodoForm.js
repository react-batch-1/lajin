import React, {Component} from 'react';


class TodoForm extends Component {

  constructor(props) {
    super(props);
    this.state = {
        task: ""
    };
  }

  randomNumber = (min, max) => {
    return Math.floor(Math.random() 
            * (max - min + 1)) + min;
  };

  addTask=(event) => {
  
   event.preventDefault();
   if(this.state.task.length) {
      const task = { 
        id:this.randomNumber(1000,9999), 
        task:this.state.task, 
        status:false
      };
      this.props.onAddTask(task);
      document.getElementById('new-todo').value = "";
      this.state.task = "";
    }
  }

  render() {
    return (
      <>
      <form onSubmit={this.addTask}>
        <input type='text' placeholder='Add your task' onChange={(e)=>{this.setState({task:e.target.value})}} 
          name='new-todo' id="new-todo" maxLength="200" size="55" />
        <button className='button'>Add Task</button>
      </form>
      
      </>
    );
  }

}

export default TodoForm;