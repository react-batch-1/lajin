import React,{useState} from 'react'

import Child from './Child';

function Parent() {
  
  const [data,setData] = useState({
    full_name: "Lajin MJ",
    email: "lajinmj@gmail.com",
    phone: "+91 9847858160",

  });

  const updateName=(name)=>{
    console.log("parent updateName()");
    setData({
        full_name: name,
        email: "lajinmj@gmail.com",
        phone: "+91 9847858160",
    })
  }
    
  return (
    <div>     
      <Child  {...data} onUpdateName={updateName} />
    </div>
  )
}

export default Parent
