import React, { useState } from 'react'

function Child({onUpdateName,full_name,email,phone}) {
    //const  {full_name,email,phone} = props;
    //const [childData,setChildData] = useState(full_name);
   
    return (
        <>
        <div>
            <h2>Name: {full_name}</h2>
            <h2>Email: {email}</h2>
            <h2>Phone: {phone}</h2>
        </div>
        <input type='text' placeholder='Enter Your Name' id="name" name="name" /><br/>
        <button onClick={()=>onUpdateName(document.getElementById('name').value)}>Sent Data back to Parent</button>
        </>
    )
}

export default Child
