import React, {useState} from 'react';

const TodoForm =(props)=>{
   const [todoItem, setTodoItem] = useState("");
    const addTask =(event)=>{
      event.preventDefault();
      if(todoItem.length>0)
      {
        props.sendTodoItem(todoItem);
        document.getElementById('new-todo').value='';
        setTodoItem("");
      }        
      else
        alert("Please enter the task to be added.");
     // console.log(props)
    }
  

    return(
      <form onSubmit={addTask}>
        <input type='text' placeholder='Add your task'  onChange={(e) => setTodoItem(e.target.value)}
          name='new-todo' id="new-todo" maxLength="200" size="55" />
        <button className='button'>Add Task</button>
      </form>
      
    );
  }

  export default TodoForm;