import React,{useState} from 'react';
import TodoForm from './TodoForm';

const TodoList=() =>{

    const [todoList, setTodoList] = useState([]);

    const randomNumber = (min, max) => {
        return Math.floor(Math.random() 
                * (max - min + 1)) + min;
    };

    const  [todoCount, setTodoCount] = useState(0);
    const  [completedCount, setCompletedCount] = useState(0);

    const receiveTodoItem = (data) => {
        //console.log(data);
        const newTask = { 
            "id":randomNumber(1000,9999),
            "task":data, 
            "active":true
        };
        //console.log(newTask);
        setTodoList([...todoList,newTask]);
        setTodoCount(todoCount+1)
        //setCompletedCount(completedCount>0?completedCount-1:0)
    };

    const completeTask=(id)=>{
        todoList.forEach((item)=>{
            if(item.id===id)
                item.active = false;
        });
        //console.log(todoList)
        setTodoList([...todoList]);
        setTodoCount(todoCount>0?todoCount-1:todoCount)
        setCompletedCount(completedCount+1)
        console.log(todoCount);
        console.log(completedCount);
    }

    return(
        <>
        <TodoForm sendTodoItem={receiveTodoItem} />
        <h2 className={(todoCount>0?'show':'hide')}>Active Tasks</h2>
        <ul>
        {
            todoList.map((item)=>{             
            return (item.active?<li>
                        {item.task}
                        <button className="edit" onClick={() => completeTask(item.id)}>Done</button>
                    </li>:"");
          })
        }
        </ul>
        {/* className='{todoList.filter(item => item.active === true}' */}
        <h2 className={(completedCount>0?'show':'hide')}>Completed Tasks</h2>
        <ul>
        {
          todoList.map((item)=>{
            return (!item.active?<li>
                {item.task}
            </li>:"");
          })
        }
        </ul>
        </>
    )
}

export default TodoList;